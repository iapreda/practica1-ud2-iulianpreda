DELIMITER //
CREATE PROCEDURE crearTablaOrdenador()
BEGIN
CREATE TABLE ordenadores(
                       id int primary key AUTO_INCREMENT,
                       numserie varchar(15) unique,
                       marca varchar(30),
                       modelo varchar (30),
                       precio float ,
                       fecha_alta timestamp );

END //
