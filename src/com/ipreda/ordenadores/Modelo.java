package com.ipreda.ordenadores;

import java.io.*;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.Properties;

public class Modelo {
    private Connection conn;

    private String dirIP;
    private String baseDatos;
    private String user;
    private String pwd;

    /**
     * Constructor de la clase
     */
    public Modelo() {
        getPropValues();
    }

    /**
     * Getters y setters utilizados en las consultas
     * @return
     */
    public String getDirIP() {
        return dirIP;
    }

    public void setDirIP(String dirIP) {
        this.dirIP = dirIP;
    }

    public String getBaseDatos() {
        return baseDatos;
    }

    public void setBaseDatos(String baseDatos) {
        this.baseDatos = baseDatos;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    /**
     * Metodo para crear la tabla de ordenadores.
     * @throws SQLException
     */
    public void crearTablaOrdenadores() throws SQLException{
        conn = null;
        conn = DriverManager.getConnection("jdbc:mysql://"+dirIP+":3306/"+baseDatos, user, pwd);

        String sentenciaSql="call crearTablaOrdenador()";
        CallableStatement procedimiento = null;
        procedimiento = conn.prepareCall(sentenciaSql);
        procedimiento.execute();

    }

    /**
     * Metodo para el apartado de configuracion
     * @param ip
     * @param baseDatos
     * @param userDB
     * @param password
     */
    void configurar(String ip,String baseDatos, String userDB, String password) {
        try {
            Properties prop = new Properties();
            prop.setProperty("dirip", ip);
            prop.setProperty("basedatos",baseDatos);
            prop.setProperty("user", userDB);
            prop.setProperty("pwd", password);
            OutputStream out = new FileOutputStream("config.properties");
            prop.store(out, null);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        this.dirIP = ip;
        this.baseDatos = baseDatos;
        this.user = userDB;
        this.pwd = password;

    }

    /**
     *
     * Metodo relacionado con las propiedades del programa
     */
    private void getPropValues() {
        InputStream inputStream = null;
        try {
            Properties prop = new Properties();
            String propFileName = "config.properties";

            inputStream = new FileInputStream(propFileName);

            prop.load(inputStream);
            dirIP = prop.getProperty("dirip");
            baseDatos=prop.getProperty("basedatos");
            user = prop.getProperty("user");
            pwd = prop.getProperty("pwd");


        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            try {
                if (inputStream != null) inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * Metodo que establece la conexion con el sql
     * @throws SQLException
     */
    public void conectar() throws SQLException {
        conn = null;
        conn = DriverManager.getConnection("jdbc:mysql://"+dirIP+":3306/"+baseDatos, user, pwd);

    }

    /**
     * Metodo que desconecta la base de datos.
     * @throws SQLException
     */
    public void desconectar() throws SQLException {
        conn.close();
        conn=null;
    }

    /**
     *
     * Metodo para obtener todos los datos de la base de datos mediante una consulta
     *  @return
     * @throws SQLException
     */
    public ResultSet obtenerDatos() throws SQLException{
        if (conn==null){
            return null;
        }
        if (conn.isClosed()){
            return null;
        }
        String consulta = "SELECT * FROM ordenadores";
        PreparedStatement sentencia = null;
        sentencia = conn.prepareStatement(consulta);
        ResultSet resultadoConsulta = sentencia.executeQuery();
        return resultadoConsulta;
    }

    /**
     * Metodo para obtener sólo las marcas
     * @return
     * @throws SQLException
     */
    public ResultSet obtenerDatos1() throws SQLException{
        if (conn==null){
            return null;
        }
        if (conn.isClosed()){
            return null;
        }
        String consulta ="SELECT marca,COUNT(*) FROM ordenadores GROUP BY marca";
        PreparedStatement sentencia = null;
        sentencia = conn.prepareStatement(consulta);
        ResultSet resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Metodo que introduce datos en la bbdd mediante consulta y parametros.
     * @param numserie
     * @param marca
     * @param modelo
     * @param precio
     * @param fecha_alta
     * @return
     * @throws SQLException
     */
    public int insertarOrdenador(String numserie, String marca, String modelo, float precio, LocalDateTime fecha_alta) throws SQLException {

        if (conn==null){
            return -1;
        }if (conn.isClosed()){
            return -2;
        }

        String consulta="INSERT INTO ordenadores(numserie, marca, modelo, precio,fecha_alta)"+
                "VALUES (?,?,?,?,?)";

        PreparedStatement sentencia = null;
        sentencia = conn.prepareStatement(consulta);
        sentencia.setString(1,numserie);
        sentencia.setString(2,marca);
        sentencia.setString(3,modelo);
        sentencia.setFloat(4,precio);
        sentencia.setTimestamp(5,Timestamp.valueOf(fecha_alta));

        int numeroRegistros = sentencia.executeUpdate();

        if (sentencia!=null){
            sentencia.close();
        }
        return numeroRegistros;
    }

    /**
     * Metodo que elimina una fila de la bbdd.
     * @param id
     * @return
     * @throws SQLException
     */
    public int eliminarOrdenador(int id) throws SQLException {
        if (conn==null){
            return -1;
        }if (conn.isClosed()){
            return -2;
        }
        String consulta="DELETE FROM ordenadores WHERE ID=?";
        PreparedStatement sentencia = null;
        sentencia = conn.prepareStatement(consulta);
        sentencia.setInt(1,id);

        int resultado = sentencia.executeUpdate();
        if (sentencia!=null){
            sentencia.close();
        }
        return  resultado;
    }

    /**
     * Metodo para modificar datos en la bbdd
     * @param id
     * @param numserie
     * @param marca
     * @param modelo
     * @param precio
     * @param fecha_alta
     * @return
     * @throws SQLException
     */
    public int modificarOrdenador(int id,String numserie, String marca, String modelo, float precio, Timestamp fecha_alta) throws SQLException{
        if (conn==null){
            return -1;
        }if (conn.isClosed()){
            return -2;
        }

        String consulta = "UPDATE ordenadores SET numserie=?, marca=?, modelo=?, precio=?,fecha_alta=? WHERE id=?";

        PreparedStatement sentencia = null;
        sentencia = conn.prepareStatement(consulta);
        sentencia.setString(1,numserie);
        sentencia.setString(2,marca);
        sentencia.setString(3,modelo);
        sentencia.setFloat(4,precio);
        sentencia.setTimestamp(5,fecha_alta);
        sentencia.setInt(6,id);

        int resultado = sentencia.executeUpdate();

        if (sentencia!=null){
            sentencia.close();
        }

        return resultado;
    }

    /**
     * Metodo para buscar datos mediante el campo clave.
     * @param numeroSerie
     * @return
     * @throws SQLException
     */
    public ResultSet buscarOrdenador(String numeroSerie) throws SQLException {
        if (conn==null) {
            return null;
        }
        if (conn.isClosed()) {
            return null;
        }
        String consulta ="SELECT * FROM ordenadores WHERE numserie  LIKE '"+numeroSerie+"'";
        PreparedStatement sentencia = null;
        sentencia=conn.prepareStatement(consulta);
        ResultSet resultado=sentencia.executeQuery();
        return resultado;
    }

    /**
     * Metodo para mostrar todos los datos despues de la consulta a un campo clave.
     * @return
     * @throws SQLException
     */
    public ResultSet mostrarTODO() throws SQLException {

        if (conn==null) {
            return null;
        }
        if (conn.isClosed()) {
            return null;
        }
        String consulta ="SELECT * FROM ordenadores";
        PreparedStatement sentencia = null;
        sentencia=conn.prepareStatement(consulta);
        ResultSet resultado=sentencia.executeQuery();
        return resultado;
    }

    /**
     * Metodo para filtrar los datos de ordenadores por marcas.
     * @throws SQLException
     */
    public void ordenadorPorMarca() throws SQLException {
        String sentenciaSql ="call mostrarOrdenadorMarca()";

        CallableStatement procedimiento=null;
        procedimiento=conn.prepareCall(sentenciaSql);
        procedimiento.execute();
    }


}
