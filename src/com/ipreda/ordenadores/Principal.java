package com.ipreda.ordenadores;

public class Principal {
    /**
     * Clase principal que arranca el programa
     * @param args
     */
    public static void main(String[] args) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();

        Controlador controlador = new Controlador(vista,modelo);
    }
}
