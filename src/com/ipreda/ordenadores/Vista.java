package com.ipreda.ordenadores;

import com.github.lgooddatepicker.components.DateTimePicker;


import javax.swing.*;
import javax.swing.table.DefaultTableModel;

/**
 * Clase .JAVA del formulario.
 */
public class Vista {
    private JPanel panel1;
    JTextField txMarca;
    JTextField txModelo;
    JTextField txPrecio;
    JButton btnBuscar;
    JButton btnNuevo;
    JButton btnEliminar;
    JTextField txBuscar;
    JLabel lblAccion;
    JTable table1;
    JButton btnOrdenadorMarca;
    private JTable ordenadorMarca;
    JTextField txNumSerie;
    DateTimePicker dateTimePicker;
    JButton btnTodo;
    JTextField txIP;
    JTextField txBBDD;
    JTextField txUsuario;
    JPasswordField txPWD;
    JButton btnConfigurar;

    DefaultTableModel dtm;
    DefaultTableModel dtm2;
    JMenuItem itemConectar;
    JMenuItem itemCrearTabla;
    JMenuItem itemSalir;
    JMenuItem itemConfiguracion;
    JFrame frame;

    public Vista(){
        frame = new JFrame("BBDD ORDENADORES");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        dtm = new DefaultTableModel();
        table1.setModel(dtm);

        dtm2 = new DefaultTableModel();
        ordenadorMarca.setModel(dtm2);

        crearMenu();
        frame.pack();
        frame.setVisible(true);

    }

    private void crearMenu(){
        itemConectar = new JMenuItem("CONECTAR");
        itemConectar.setActionCommand("Conectar");
        itemCrearTabla = new JMenuItem("CREAR TABLA ORDENADORES");
        itemCrearTabla.setActionCommand("CrearTablaOrdenadores");
        itemSalir = new JMenuItem("SALIR");
        itemSalir.setActionCommand("Salir");

        JMenu menuArchivo = new JMenu("Archivo");
        menuArchivo.add(itemConectar);
        menuArchivo.add(itemCrearTabla);
        menuArchivo.add(itemSalir);


        JMenuBar barraMenu = new JMenuBar();
        barraMenu.add(menuArchivo);
        frame.setJMenuBar(barraMenu);

    }


}
