package com.ipreda.ordenadores;



import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * Clase publica controlador
 */
public class Controlador implements ActionListener, TableModelListener {

    private Vista vista;
    private Modelo modelo;

    private enum estadoDB {conectado, desconectado}


    private estadoDB estado;

    /**
     * Constructor de la clase controlador
     * @param vista
     * @param modelo
     */
    public Controlador(Vista vista, Modelo modelo) {

        this.modelo = modelo;
        this.vista = vista;
        estado = estadoDB.desconectado;
        setOptions();
        iniciarTabla();
        iniciarTabla1();

        addActionListener(this);
        addTableModelListener(this);

    }

    /**
     * Metodo para agregar el listener a table model
     * @param controlador
     */
    private void addTableModelListener(Controlador controlador) {

        vista.dtm.addTableModelListener(controlador);
        vista.dtm2.addTableModelListener(controlador);


    }

    /**
     * Metodo para agregar listener al controlador
     * @param controlador
     */
    private void addActionListener(Controlador controlador) {
        vista.btnBuscar.addActionListener(controlador);
        vista.btnEliminar.addActionListener(controlador);
        vista.btnNuevo.addActionListener(controlador);
        vista.btnOrdenadorMarca.addActionListener(controlador);
        vista.itemConectar.addActionListener(controlador);
        vista.itemCrearTabla.addActionListener(controlador);
        vista.itemSalir.addActionListener(controlador);
        vista.btnTodo.addActionListener(controlador);
        vista.btnConfigurar.addActionListener(controlador);
    }

    /**
     * Metodo para iniciar una de las dos tablas dtm
     */
    private void iniciarTabla1() {
        String[] headers = {"marca", "CANTIDAD"};
        vista.dtm2.setColumnIdentifiers(headers);
    }

    /**
     * Metodo para cargar datos en una tabla dtm
     * @param resultSet
     * @throws SQLException
     */
    private void cargarFilas1(ResultSet resultSet) throws SQLException {
        Object[] fila = new Object[2];
        vista.dtm2.setRowCount(0);

        while (resultSet.next()) {
            fila[0] = resultSet.getObject(1);
            fila[1] = resultSet.getObject(2);

            vista.dtm2.addRow(fila);
        }

        if (resultSet.last()) {
            vista.lblAccion.setVisible(true);
            vista.lblAccion.setText(resultSet.getRow() + " FILAS CARGADAS");
        }
    }

    /**
     * Cuerpo de la clase controlador.
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        String comando = e.getActionCommand();

        switch (comando) {
            case "Ordenadores por Marca":
                try {
                    modelo.ordenadorPorMarca();
                    cargarFilas1(modelo.obtenerDatos1());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;

            case "CrearTablaOrdenadores":
                try {
                    modelo.crearTablaOrdenadores();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;

            case "Nuevo":
                try {
                    modelo.insertarOrdenador(vista.txNumSerie.getText(), vista.txMarca.getText(), vista.txModelo.getText(), Float.parseFloat(vista.txPrecio.getText()), vista.dateTimePicker.getDateTimePermissive());
                    cargarFilas(modelo.obtenerDatos());
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
                break;
            case "Buscar":

                try {
                    cargarFilas(modelo.buscarOrdenador(vista.txBuscar.getText()));
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }

                break;

            case "Eliminar":
                try {
                    int filaBorrar = vista.table1.getSelectedRow();
                    int idBorrar = (int) vista.dtm.getValueAt(filaBorrar, 0);
                    modelo.eliminarOrdenador(idBorrar);
                    vista.dtm.removeRow(filaBorrar);
                    vista.lblAccion.setText("Borrado.");
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;

            case "Salir":
                System.exit(0);
                break;


            case "Todo":
                try {
                    modelo.mostrarTODO();
                    cargarFilas(modelo.obtenerDatos());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;

            case "Configurar":
                modelo.configurar(vista.txIP.getText(), vista.txBBDD.getText(), vista.txUsuario.getText(), vista.txPWD.getText());
                break;

            case "Conectar":
                if (estado == estadoDB.desconectado) {
                    try {
                        modelo.conectar();
                        vista.itemConectar.setText("Desconectar");
                        estado = estadoDB.conectado;
                        cargarFilas(modelo.obtenerDatos());
                    } catch (SQLException e1) {
                        JOptionPane.showMessageDialog(null, "ERROR DE CONEXION", "ERROR", JOptionPane.ERROR_MESSAGE);
                        e1.printStackTrace();
                    }
                } else {
                    try {
                        modelo.desconectar();
                        vista.itemConectar.setText("Conectar");
                        estado = estadoDB.desconectado;
                        vista.lblAccion.setText("Desconectado");
                    } catch (SQLException throwables) {
                        JOptionPane.showMessageDialog(null, "ERROR DE  DESCONEXION", "ERROR", JOptionPane.ERROR_MESSAGE);
                        throwables.printStackTrace();
                    }
                }

        }
    }

    /**
     * Metodo para cargar filas en una de las dos dtm
     * @param obtenerDatos
     * @throws SQLException
     */
    private void cargarFilas(ResultSet obtenerDatos) throws SQLException {
        Object[] fila = new Object[6];
        vista.dtm.setRowCount(0);
        while (obtenerDatos.next()) {
            fila[0] = obtenerDatos.getObject(1);
            fila[1] = obtenerDatos.getObject(2);
            fila[2] = obtenerDatos.getObject(3);
            fila[3] = obtenerDatos.getObject(4);
            fila[4] = obtenerDatos.getObject(5);
            fila[5] = obtenerDatos.getObject(6);

            vista.dtm.addRow(fila);
        }
        if (obtenerDatos.last()) {
            vista.lblAccion.setVisible(true);
            vista.lblAccion.setText(obtenerDatos.getRow() + " FILAS CARGADAS");
        }
    }

    /**
     * Metodo para formatear el header de una de las tablas
     */
    private void iniciarTabla() {
        String[] headers = {"id", "numserie", "marca", "modelo", "precio", "fecha alta"};
        vista.dtm.setColumnIdentifiers(headers);
    }

    /**
     * Metodo para las opciones
     */
    private void setOptions() {
        vista.txIP.setText(modelo.getDirIP());
        vista.txBBDD.setText(modelo.getBaseDatos());
        vista.txUsuario.setText(modelo.getUser());
        vista.txPWD.setText(modelo.getPwd());
    }

    /**
     * "Observador" que actualiza las tablas en caso de cambio.
     * @param e
     */
    @Override
    public void tableChanged(TableModelEvent e) {

        if (e.getType() == TableModelEvent.UPDATE) {
            System.out.println("Tabla actualizada");
            int filaModificada = e.getFirstRow();

            try {
                modelo.modificarOrdenador((Integer) vista.dtm.getValueAt(filaModificada, 0), (String) vista.dtm.getValueAt(filaModificada, 1), (String) vista.dtm.getValueAt(filaModificada, 2), (String) vista.dtm.getValueAt(filaModificada, 3), (Float) vista.dtm.getValueAt(filaModificada, 4), Timestamp.valueOf((String) vista.dtm.getValueAt(filaModificada, 5)));
                vista.lblAccion.setText("Actualización completa");
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }


    }
}
